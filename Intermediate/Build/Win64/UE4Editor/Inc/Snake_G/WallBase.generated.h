// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_G_WallBase_generated_h
#error "WallBase.generated.h already included, missing '#pragma once' in WallBase.h"
#endif
#define SNAKE_G_WallBase_generated_h

#define Snake_G_Source_Snake_G_WallBase_h_13_SPARSE_DATA
#define Snake_G_Source_Snake_G_WallBase_h_13_RPC_WRAPPERS
#define Snake_G_Source_Snake_G_WallBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_G_Source_Snake_G_WallBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWallBase(); \
	friend struct Z_Construct_UClass_AWallBase_Statics; \
public: \
	DECLARE_CLASS(AWallBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_G"), NO_API) \
	DECLARE_SERIALIZER(AWallBase) \
	virtual UObject* _getUObject() const override { return const_cast<AWallBase*>(this); }


#define Snake_G_Source_Snake_G_WallBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAWallBase(); \
	friend struct Z_Construct_UClass_AWallBase_Statics; \
public: \
	DECLARE_CLASS(AWallBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_G"), NO_API) \
	DECLARE_SERIALIZER(AWallBase) \
	virtual UObject* _getUObject() const override { return const_cast<AWallBase*>(this); }


#define Snake_G_Source_Snake_G_WallBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWallBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWallBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallBase(AWallBase&&); \
	NO_API AWallBase(const AWallBase&); \
public:


#define Snake_G_Source_Snake_G_WallBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallBase(AWallBase&&); \
	NO_API AWallBase(const AWallBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWallBase)


#define Snake_G_Source_Snake_G_WallBase_h_13_PRIVATE_PROPERTY_OFFSET
#define Snake_G_Source_Snake_G_WallBase_h_10_PROLOG
#define Snake_G_Source_Snake_G_WallBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_G_Source_Snake_G_WallBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Snake_G_Source_Snake_G_WallBase_h_13_SPARSE_DATA \
	Snake_G_Source_Snake_G_WallBase_h_13_RPC_WRAPPERS \
	Snake_G_Source_Snake_G_WallBase_h_13_INCLASS \
	Snake_G_Source_Snake_G_WallBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_G_Source_Snake_G_WallBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_G_Source_Snake_G_WallBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Snake_G_Source_Snake_G_WallBase_h_13_SPARSE_DATA \
	Snake_G_Source_Snake_G_WallBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_G_Source_Snake_G_WallBase_h_13_INCLASS_NO_PURE_DECLS \
	Snake_G_Source_Snake_G_WallBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_G_API UClass* StaticClass<class AWallBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_G_Source_Snake_G_WallBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
