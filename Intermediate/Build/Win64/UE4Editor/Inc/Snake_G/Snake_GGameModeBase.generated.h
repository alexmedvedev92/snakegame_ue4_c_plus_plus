// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_G_Snake_GGameModeBase_generated_h
#error "Snake_GGameModeBase.generated.h already included, missing '#pragma once' in Snake_GGameModeBase.h"
#endif
#define SNAKE_G_Snake_GGameModeBase_generated_h

#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_SPARSE_DATA
#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_RPC_WRAPPERS
#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnake_GGameModeBase(); \
	friend struct Z_Construct_UClass_ASnake_GGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnake_GGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_G"), NO_API) \
	DECLARE_SERIALIZER(ASnake_GGameModeBase)


#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnake_GGameModeBase(); \
	friend struct Z_Construct_UClass_ASnake_GGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnake_GGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_G"), NO_API) \
	DECLARE_SERIALIZER(ASnake_GGameModeBase)


#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake_GGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake_GGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake_GGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake_GGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake_GGameModeBase(ASnake_GGameModeBase&&); \
	NO_API ASnake_GGameModeBase(const ASnake_GGameModeBase&); \
public:


#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake_GGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake_GGameModeBase(ASnake_GGameModeBase&&); \
	NO_API ASnake_GGameModeBase(const ASnake_GGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake_GGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake_GGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake_GGameModeBase)


#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_12_PROLOG
#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_SPARSE_DATA \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_RPC_WRAPPERS \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_INCLASS \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_SPARSE_DATA \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Snake_G_Source_Snake_G_Snake_GGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_G_API UClass* StaticClass<class ASnake_GGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_G_Source_Snake_G_Snake_GGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
